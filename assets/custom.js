/**
 * Include your custom JavaScript here.
 *
 * We also offer some hooks so you can plug your own logic. For instance, if you want to be notified when the variant
 * changes on product page, you can attach a listener to the document:
 *
 * document.addEventListener('variant:changed', function(event) {
 *   var variant = event.detail.variant; // Gives you access to the whole variant details
 * });
 *
 * You can also add a listener whenever a product is added to the cart:
 *
 * document.addEventListener('product:added', function(event) {
 *   var variant = event.detail.variant; // Get the variant that was added
 *   var quantity = event.detail.quantity; // Get the quantity that was added
 * });
 *
 * If you just want to force refresh the mini-cart without adding a specific product, you can trigger the event
 * "cart:refresh" in a similar way (in that case, passing the quantity is not necessary):
 *
 * document.documentElement.dispatchEvent(new CustomEvent('cart:refresh', {
 *   bubbles: true
 * }));
 */

$('document').ready(function() {

    $('.ptab:first-child').addClass('active');
    $('.pcontent:first-child').addClass('active');

    $('.ptab').click(function() {
        $('.pcontent').removeClass('active');
        $('.ptab').removeClass('active');
        $('[data-content=' + $(this).data('tab') + ']').addClass('active');
        $(this).addClass('active');
     });

    $('.play-icon').click(function() {
        $(".c-video > iframe")[0].src += "&autoplay=1";
        $(".c-video, .play-icon").addClass('active');
    });


    // ACCORDION SECTION
    $(window).on('load', function () {
        $('.dropdown:first-of-type > .dropdown-body').trigger('click');
    });
  
    $('.dropdown').click(function () {
    
        if ($(this).hasClass('shown')) {
        $(this).removeClass('shown');
        $(this).find('.dropdown-body').slideUp(350);
        }
    
        else {
        $('.dropdown').removeClass('shown');
        $('.dropdown > .dropdown-body').slideUp(350);
        $(this).addClass('shown');
        $(this).find('.dropdown-body').slideDown(350);
        }
    
    });
    
    $('.ing-trigger').click(function() {
        
        if ($('#ing-popup').hasClass('active')) {
            $('#ing-popup').removeClass('active');
        }
        else{
            $('#ing-popup').addClass('active');
        }
    });

    var maxHeight = -1;

    $('.card').each(function() {
    maxHeight = maxHeight > $(this).height() ? maxHeight : $(this).height();
    });

    $('.card').each(function() {
    $(this).height(maxHeight);
    });
});
